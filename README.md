<h1 align="center">
  <br>
  <a href="https://gitlab.com/usamahamed/documentation-tryout"><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ83frxv5z5wQeXS7_D4dsEyMsIDS7_RNTQX2XUEyDP7FFobqy5" alt="netent" width="200"></a>
  <br>
  simple Scripting Try-Out
  <br>
</h1>

<h4 align="center">A simple scripting for a basic product
<p align="center">
  <a href="https://gitlab.com/usamahamed/documentation-tryout">
    <img src="https://badge.fury.io/js/electron-markdownify.svg"
         alt="Gitter">
  </a>
  <a href="https://gitlab.com/usamahamed/documentation-tryout"><img src="https://badges.gitter.im/amitmerchant1990/electron-markdownify.svg"></a>
  <a href="https://github.com/usamahamed/Chat-App">
      <img src="https://img.shields.io/badge/SayThanks.io-%E2%98%BC-1EAEDB.svg">
  </a>
  <a href="https://gitlab.com/usamahamed/documentation-tryout">
    <img src="https://img.shields.io/badge/$-donate-ff69b4.svg?maxAge=2592000&amp;style=flat">
  </a>
  <a href="https://gitlab.com/usamahamed/documentation-tryout"><img src="https://img.shields.io/circleci/project/vuejs/vue/dev.svg" alt="Build Status"></a>
  <a href="https://gitlab.com/usamahamed/documentation-tryout"><img src="https://img.shields.io/codecov/c/github/vuejs/vue/dev.svg" alt="Coverage Status"></a>
  <a href="https://gitlab.com/usamahamed/documentation-tryout"><img src="https://img.shields.io/npm/dm/vue.svg" alt="Downloads"></a>
  <a href="https://gitlab.com/usamahamed/documentation-tryout"><img src="https://img.shields.io/npm/v/vue.svg" alt="Version"></a>
  <a href="https://gitlab.com/usamahamed/documentation-tryout"><img src="https://img.shields.io/npm/l/vue.svg" alt="License"></a>
  <a href="https://gitlab.com/usamahamed/documentation-tryout"><img src="https://img.shields.io/badge/chat-on%20discord-7289da.svg" alt="Chat"></a>

</p>


## Prerequisites
### Steps 

* Create configurable product in RuAD
* Add simple configuration to the product
* embedding it into a webshop 
* calculating a price

## Getting Started

### First step is to create a configurable product in Admin interface
more info about that here: https://docs.roomle.com/web/datamanagement/products.html#create-a-configurable-product

### Second step is to scripting the product
After you successfully created a configurable product from step1 (https://admin.roomle.com/vizmotor/catalogs/brands_3/products/brands_3:eames_plastic_chair `just for examples this maybe our samples`) and your script should now have the component id and it is time to customize this product as you like.
#### There is two ways to do that:

* Simply by the configurator
In the Admin interface there is a section `Configuration definition` you can use the configurator to change material colors, contents shapes, dimentions, etc and after you finished your product configuration you can just click save and this configuration is saved to the product so whenever you upload the product from RuAD or from the configurator outside, it will reload with the updated configurations.

* From configuration script
If you already have a scripting knowledge you can do it also in the admin interface in the same section `Configuration definition` you can simply click on the button `show script` and it will show you a textarea with the configuration script, something like this:
```
    {
        "componentId": "brands_3:Eames_Plastic_Chairs"
    }
```
and if you want change anything you can add a it inside the script like this: (`note: scripter team could provide a simple configuration change example here`)
```
    "parameters": {
        "BaseOfLegs": "hard",
        "BaseOfLegs02": "brands_3:02golden_maple",
        "BaseType": "wire",
        "Fabrics": "brands_3:05dark_grey",
        "LegsMaterial": "brands_3:01_basic_dark",
        "Piped": "brands_3:01_basic_dark",
        "Seat": "brands_3:04_white",
        "Shell": "ArmchairWithSeat",
        "glides": "brands_3:glides_basic_dark"
    }
```
After you finish, click `save` button. For more info about scripting you can check the [scripting materials](https://docs.roomle.com/scripting/resources/#scripting-course)


### Third step is embedding
Now after step1 and step2 done successfully, then the product now is ready to be use in your webshop. 

* In admin interface go inside the product detail page and navigate to the `Integration links` section in the page bottom.
* Inside the section there is `Embedding code` that include the code that you can simply copy and paste it inside your website.
* If you don't have idea where to put this code or you want to preview how the product should look in the webshop you can click on `GENERATE SANDBOX` button and then you will navigate to code sandbox service which you can see the static html files including your item and the preview like here: https://codesandbox.io/s/twd3y?file=/index.html (`Note: we can change this link`)
* You can also see whole integration example which is helpful if you want to deeply integrate the product into your webshop. In the same section there is `Integration example` and then click on `GENERATE SANDBOX` button and you will redirect also to  code sandbox service. like here https://codesandbox.io/s/twd3y?file=/index.html (`Note: we can change this link`).

### Last step is calculating a price
After step3 done successfully and you saw the integration example of integration, the last step is to calculate the price of the intgrated product. In the integration example we show in step3, in the left side there is a file `index.js` this contains an instance of configurator which you can use all configurator controls but what matter for this step is the method `onPartListUpdate` this return `partList` and inside this object it include all the article numbers for the product. Basically each product is consist of an object of partList and inside each entry it include article number and its price like in this example: 

```
fullList: {
    0: {
        articleNr: N2998
        componentId: "brands_3:Eames_Plastic_Chairs"
        label: "Eames Plastic Chairs"
        labelIsCalculated: false
        count: 1
        packageSize: 1
        valid: true
        price: 15
        retailerPrice: 0
        currencySymbol: $
    }

}
```
This is example of one entry inside the partList which contains the price of the articleNr of the componentId `brands_3:Eames_Plastic_Chairs` and the `count` represent the number of `articleNr` used inside the product which affecting on calculating the total price for article numbers which is the price for the whole product. But what if you want to use your own prices for those article numbers ?

There is two ways for getting the price:

### Use roomle prices
If you want the existing prices in roomle server, you can use the existing price inside the `partList` objects or you can also upload your own prices to our servers from admin interface. you can do that by doing the following: 

* Navigate to catalogs and select your catalog that include your products.
* Click on the `Import/Export` in the menu on the top.
* Scroll in the page and you will see the `Import Prices` and beside it `Export Prices`.
* If you already has the price file with extentsion `.csv` you can drag/drop it in `Import Prices` uploader.
* If you don't have idea of the formating of the file, you can download the example inside the uploader `prices.csv`.
* You can also export the existing prices from `Export Prices` section and you can modify something and re-import it again.

After you doing this, our service will mapping the new prices with the article numbers.

### Use prices on your own servers
If the prices already hosted on your own servers, you can also easily fetching it and calculate the total price. In the integration example as we described above inside the method `onPartListUpdate` you can fetch your prices there from your servers and calculating it. In this [example](https://codesandbox.io/s/xenodochial-silence-rnk15?file=/index.js:1345-1350) you can see a simple calculation of the total price, to understad the example it is contain from 3 steps:

* The method `fetchPrice` this simulate the request to your server to get your prices. you can use your own endPoint there.
* The variable `promises` is simulate the mapping between the prices you get from step1 and the `partList` object. 
* The variable `price` is simple function to calculate the total prices after mapping process is done on step2. 

Then you have your total price which is ready to put it on your shopping cart. 

